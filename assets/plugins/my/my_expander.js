(function( $ ) {
	var settings = {
		target			: null,
		speed 			: 300,
		replaceText		: null,
		defaultText		: null
	}

	var _ = {
		_replaceText: function ( a, s ) {
			if ( s.replaceText != null ) {
				a.text( a.text() == s.defaultText ? s.replaceText : s.defaultText );
			}
		}
	}

	var methods = {
		init : function( options ) {
			return this.each(function(){
				var $this 	= $(this);
				var s 		= $this.data("settings");

				s.defaultText = $this.find('a').text();

				if ( s.target == null ) {
					console.warn( "Не задан параметр `target`" );
				}

				$this.on('click', function () {
					var a = $(this).find('a');

					if ( s.target != null ) {
						s.target.slideToggle(s.speed);
					}

					a.toggleClass('active');

					_._replaceText(a, s);
					return false;
				});
			});
		},

		show : function( ) {
			var s = this.data("settings");

			return this.each(function() {
				var a = $(this).find('a');
				
				if ( s.target != null ) {
					s.target.slideDown( s.speed );
				}

				a.addClass('active');
				_._replaceText(a, s);
			});
		},

		hide : function( ) {
			var s = this.data("settings");

			return this.each(function() {
				var a = $(this).find('a');

				if ( s.target != null ) {
					s.target.slideUp( s.speed );
				}
				
				a.removeClass('active');
				_._replaceText(a, s);
			});
		},
	};

	$.fn.my_expander = function( method ) {
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			this.data("settings", $.extend({}, settings, method));
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Метод с именем ' +  method + ' не существует для jQuery.my_expander' );
		} 

	};
})(jQuery);