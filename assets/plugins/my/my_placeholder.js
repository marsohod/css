(function( $ ) {
  $.fn.my_placeholder = function( text ) {

  	return this.each(function() {
  		var $this 	= $(this);
  		
      $this.val(text);

  		$this
        .on('focusin', function( event ) {
  	  	  console.log("focus");
          if ( $(this).val() == text ) {
            $(this).val("");
          }
	  	  })
        .on('focusout', function( event ) {
          console.log("focusout");
          if ( $(this).val() == "" ) {
            $this.val(text);
          }
        });
  	});
      
  };
})(jQuery);