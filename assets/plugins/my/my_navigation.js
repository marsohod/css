(function( $ ) {
  $.fn.my_navigation = function() {

  	return this.each(function(){
  		var $this 	= $(this);
  		var $li     = $this.find('li');

  		$li.on('click', function( event ) {
	  		$li.removeClass('active');
	  		$(this).addClass('active');
	  	});
  	});
      
  };
})(jQuery);