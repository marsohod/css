$(function(){
	var resizeEvt,
		$inputSearch 			= $("#input__search"),
		$navigations 			= $(".nav_horizontal"),
		$expandFind 			= $("#a_expand_find"),
		$expandCarModels 		= $("#a_expand_car_models"),
		$carsModelsList			= $('#cars_type__part ul'),
		$selects 				= $(".select2"),
		$navigationCar			= $('#nav_tabs_cars');
		$bSearch 				= $('.b_search__top');
	var carsModelsListHeight 	= $carsModelsList.height();


	/* On load page set focus */
	$inputSearch.my_placeholder("Поиск среди 18.565 объявлений").focus();	


	/* init all navigations for correct click by item and change active state */
	$navigations.my_navigation();
	$expandFind.my_expander({target: $('.b_search__top_hidden'), replaceText: "свернуть"});


	/* init all select on page */
	$selects.select2({
		dropdownAutoWidth 	: true,
	}).on('select2:open', function() {
	    $('.select2-container--open').find('.select2-search__field').attr('placeholder', $(this).data('placeholder-input'));
	});
});