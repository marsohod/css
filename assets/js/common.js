$(function(){
	if(navigator.userAgent.indexOf('Mac') > 0)
		$('body').addClass('mac-os');
	if(navigator.userAgent.indexOf('Windows') > 0)
		$('body').addClass('windows');
	
	if(navigator.userAgent.indexOf('Chrome') > 0) {
		$('body').addClass('chrome');
	} else if(navigator.userAgent.indexOf('Safari') > 0) {
		$('body').addClass('safari');
	}

	if(navigator.userAgent.indexOf('Firefox') > 0)
		$('body').addClass('firefox');	
	if(navigator.userAgent.indexOf('Opera') > 0 || navigator.userAgent.indexOf('OPR') > 0)
		$('body').addClass('opera');	
});