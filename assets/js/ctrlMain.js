$(function(){
	var resizeEvt,
		$inputSearch 			= $("#input__search"),
		$navigations 			= $(".nav_horizontal"),
		$expandFind 			= $("#a_expand_find"),
		$expandCarModels 		= $("#a_expand_car_models"),
		$carsModelsList			= $('#cars_type__part ul'),
		$selects 				= $(".select2"),
		$navigationCar			= $('#nav_tabs_cars');
		$bSearch 				= $('.b_search__top');
	var carsModelsListHeight 	= $carsModelsList.height();


	/* On load page set focus */
	$inputSearch.my_placeholder("Поиск среди 18.565 объявлений").focus();	


	/* init all navigations for correct click by item and change active state */
	$navigations.my_navigation();
	$expandFind.my_expander({target: $('.b_search__top_hidden'), replaceText: "свернуть"});


	/* init expand cars list link click */
	$expandCarModels
		.my_expander({
			target		: null,
			replaceText : "Скрыть все модели"
		})
		.on("click.direct", function () {
			var height 		= 0,
				heightNew 	= 0; 

			$carsModelsList
				.eq(0)
					.children()
						.each( function () {
							height += $(this).height();

							if ( $(this).is(":last-child") ) {
								height += $(this).height();
							}
						});

			heightNew = carsModelsListHeight == $carsModelsList.height() ? height : carsModelsListHeight

			$carsModelsList.css({
				"max-height"	: heightNew,
				"height" 		: heightNew == carsModelsListHeight ? null : heightNew 	//set height if expand div for transition. If click for to do collapse set null.
			});
		});


	/* init all select on page */
	$selects.select2({
		dropdownAutoWidth 	: true,
	}).on('select2:open', function() {
	    $('.select2-container--open').find('.select2-search__field').attr('placeholder', $(this).data('placeholder-input'));
	});


	/* init navigation like carousel*/
	$navigationCar.carousel({
		speed				: 200,
		pagination			: false,
		itemsPerTransition	: 1,
		prevActionElement	: $('.nav_horizontal__tail__handler__l'),
		nextActionElement	: $('.nav_horizontal__tail__handler__r')
	});


	/* init resize event for carousel resize */
	$(window).on("resize", function () {
		var hprW	= $('header .page_right').width();

		if ( hprW != $navigationCar.width() ) {
			$navigationCar.width( hprW ).parent().width(hprW);
		}

		clearTimeout(resizeEvt);
			resizeEvt = setTimeout(function() {
			$navigationCar.carousel('refresh');
		}, 250);

	}).trigger("resize");
});